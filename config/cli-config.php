<?php

// Doctrine CLI configuration file

// replace with file to your own project bootstrap
require_once __DIR__.'/../src/bootstrap.php';

return new \Symfony\Component\Console\Helper\HelperSet(array(
    'db' => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper($entityManager->getConnection()),
    'em' => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper($entityManager)
));


//return  \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);