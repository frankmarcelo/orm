<?php

// App configuration
$dbParams = [
	'driver' => 'pdo_mysql',
	'host' => 'localhost',
	'dbname' => 'blog',
	'user' => 'root',
	'password' => ''
];
// Dev mode?
$dev = true;

return $dbParams;