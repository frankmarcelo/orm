<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161106234803 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Article (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ArticleAttribute (article_id INT NOT NULL, attribute VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_149CA72E7294869C (article_id), PRIMARY KEY(article_id, attribute)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Author (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, dtype VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Bugs (id INT AUTO_INCREMENT NOT NULL, engineer_id INT DEFAULT NULL, reporter_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, created DATETIME NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_A1D338F7F8D8CDF1 (engineer_id), INDEX IDX_A1D338F7E1CFE6F5 (reporter_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE BugProduct (bug_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_9844729CFA3DB3D5 (bug_id), INDEX IDX_9844729C4584665A (product_id), PRIMARY KEY(bug_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Car (name VARCHAR(255) NOT NULL, year INT NOT NULL, PRIMARY KEY(name, year)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Comment (id INT AUTO_INCREMENT NOT NULL, post_id INT DEFAULT NULL, author_id INT DEFAULT NULL, body LONGTEXT NOT NULL, publicationDate DATETIME NOT NULL, INDEX IDX_5BC96BF04B89032C (post_id), INDEX IDX_5BC96BF0F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE CommentAuthor (id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Customer (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ExchangeMarkets (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `Order` (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, payed TINYINT(1) NOT NULL, shipped TINYINT(1) NOT NULL, created DATETIME NOT NULL, INDEX IDX_34E8BC9C9395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE OrderItem (order_id INT NOT NULL, product_id INT NOT NULL, amount INT NOT NULL, offeredPrice NUMERIC(10, 0) NOT NULL, INDEX IDX_33E85E198D9F6D38 (order_id), INDEX IDX_33E85E194584665A (product_id), PRIMARY KEY(order_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Post (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, body LONGTEXT NOT NULL, publicationDate DATETIME NOT NULL, INDEX IDX_FAB8C3B3F675F31B (author_id), INDEX publication_date_idx (publicationDate), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE PostTag (post_id INT NOT NULL, tag_name VARCHAR(255) NOT NULL, INDEX IDX_D9B10EE34B89032C (post_id), INDEX IDX_D9B10EE3B02CC1B0 (tag_name), PRIMARY KEY(post_id, tag_name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE PostAuthor (id INT NOT NULL, bio LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Products (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ExchangeStocks (id INT AUTO_INCREMENT NOT NULL, market_id INT DEFAULT NULL, symbol VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_830453FECC836F9 (symbol), INDEX IDX_830453F622F3F37 (market_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Tag (name VARCHAR(255) NOT NULL, PRIMARY KEY(name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Users (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ArticleAttribute ADD CONSTRAINT FK_149CA72E7294869C FOREIGN KEY (article_id) REFERENCES Article (id)');
        $this->addSql('ALTER TABLE Bugs ADD CONSTRAINT FK_A1D338F7F8D8CDF1 FOREIGN KEY (engineer_id) REFERENCES Users (id)');
        $this->addSql('ALTER TABLE Bugs ADD CONSTRAINT FK_A1D338F7E1CFE6F5 FOREIGN KEY (reporter_id) REFERENCES Users (id)');
        $this->addSql('ALTER TABLE BugProduct ADD CONSTRAINT FK_9844729CFA3DB3D5 FOREIGN KEY (bug_id) REFERENCES Bugs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE BugProduct ADD CONSTRAINT FK_9844729C4584665A FOREIGN KEY (product_id) REFERENCES Products (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE Comment ADD CONSTRAINT FK_5BC96BF04B89032C FOREIGN KEY (post_id) REFERENCES Post (id)');
        $this->addSql('ALTER TABLE Comment ADD CONSTRAINT FK_5BC96BF0F675F31B FOREIGN KEY (author_id) REFERENCES CommentAuthor (id)');
        $this->addSql('ALTER TABLE CommentAuthor ADD CONSTRAINT FK_2ADFE266BF396750 FOREIGN KEY (id) REFERENCES Author (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE `Order` ADD CONSTRAINT FK_34E8BC9C9395C3F3 FOREIGN KEY (customer_id) REFERENCES Customer (id)');
        $this->addSql('ALTER TABLE OrderItem ADD CONSTRAINT FK_33E85E198D9F6D38 FOREIGN KEY (order_id) REFERENCES `Order` (id)');
        $this->addSql('ALTER TABLE OrderItem ADD CONSTRAINT FK_33E85E194584665A FOREIGN KEY (product_id) REFERENCES Products (id)');
        $this->addSql('ALTER TABLE Post ADD CONSTRAINT FK_FAB8C3B3F675F31B FOREIGN KEY (author_id) REFERENCES PostAuthor (id)');
        $this->addSql('ALTER TABLE PostTag ADD CONSTRAINT FK_D9B10EE34B89032C FOREIGN KEY (post_id) REFERENCES Post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE PostTag ADD CONSTRAINT FK_D9B10EE3B02CC1B0 FOREIGN KEY (tag_name) REFERENCES Tag (name)');
        $this->addSql('ALTER TABLE PostAuthor ADD CONSTRAINT FK_E87DBE5DBF396750 FOREIGN KEY (id) REFERENCES Author (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ExchangeStocks ADD CONSTRAINT FK_830453F622F3F37 FOREIGN KEY (market_id) REFERENCES ExchangeMarkets (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ArticleAttribute DROP FOREIGN KEY FK_149CA72E7294869C');
        $this->addSql('ALTER TABLE CommentAuthor DROP FOREIGN KEY FK_2ADFE266BF396750');
        $this->addSql('ALTER TABLE PostAuthor DROP FOREIGN KEY FK_E87DBE5DBF396750');
        $this->addSql('ALTER TABLE BugProduct DROP FOREIGN KEY FK_9844729CFA3DB3D5');
        $this->addSql('ALTER TABLE Comment DROP FOREIGN KEY FK_5BC96BF0F675F31B');
        $this->addSql('ALTER TABLE `Order` DROP FOREIGN KEY FK_34E8BC9C9395C3F3');
        $this->addSql('ALTER TABLE ExchangeStocks DROP FOREIGN KEY FK_830453F622F3F37');
        $this->addSql('ALTER TABLE OrderItem DROP FOREIGN KEY FK_33E85E198D9F6D38');
        $this->addSql('ALTER TABLE Comment DROP FOREIGN KEY FK_5BC96BF04B89032C');
        $this->addSql('ALTER TABLE PostTag DROP FOREIGN KEY FK_D9B10EE34B89032C');
        $this->addSql('ALTER TABLE Post DROP FOREIGN KEY FK_FAB8C3B3F675F31B');
        $this->addSql('ALTER TABLE BugProduct DROP FOREIGN KEY FK_9844729C4584665A');
        $this->addSql('ALTER TABLE OrderItem DROP FOREIGN KEY FK_33E85E194584665A');
        $this->addSql('ALTER TABLE PostTag DROP FOREIGN KEY FK_D9B10EE3B02CC1B0');
        $this->addSql('ALTER TABLE Bugs DROP FOREIGN KEY FK_A1D338F7F8D8CDF1');
        $this->addSql('ALTER TABLE Bugs DROP FOREIGN KEY FK_A1D338F7E1CFE6F5');
        $this->addSql('DROP TABLE Article');
        $this->addSql('DROP TABLE ArticleAttribute');
        $this->addSql('DROP TABLE Author');
        $this->addSql('DROP TABLE Bugs');
        $this->addSql('DROP TABLE BugProduct');
        $this->addSql('DROP TABLE Car');
        $this->addSql('DROP TABLE Comment');
        $this->addSql('DROP TABLE CommentAuthor');
        $this->addSql('DROP TABLE Customer');
        $this->addSql('DROP TABLE ExchangeMarkets');
        $this->addSql('DROP TABLE `Order`');
        $this->addSql('DROP TABLE OrderItem');
        $this->addSql('DROP TABLE Post');
        $this->addSql('DROP TABLE PostTag');
        $this->addSql('DROP TABLE PostAuthor');
        $this->addSql('DROP TABLE Products');
        $this->addSql('DROP TABLE ExchangeStocks');
        $this->addSql('DROP TABLE Tag');
        $this->addSql('DROP TABLE Users');
    }
}
