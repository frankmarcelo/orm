<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161114223125 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Address (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Cart (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, name LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_AB9127899395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Student (id INT AUTO_INCREMENT NOT NULL, mentor_id INT DEFAULT NULL, name LONGTEXT NOT NULL, UNIQUE INDEX UNIQ_789E96AFDB403044 (mentor_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Cart ADD CONSTRAINT FK_AB9127899395C3F3 FOREIGN KEY (customer_id) REFERENCES Customer (id)');
        $this->addSql('ALTER TABLE Student ADD CONSTRAINT FK_789E96AFDB403044 FOREIGN KEY (mentor_id) REFERENCES Student (id)');
        $this->addSql('ALTER TABLE Article ADD author_id INT DEFAULT NULL, ADD headline VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE Article ADD CONSTRAINT FK_CD8737FAF675F31B FOREIGN KEY (author_id) REFERENCES Users (id)');
        $this->addSql('CREATE INDEX IDX_CD8737FAF675F31B ON Article (author_id)');
        $this->addSql('ALTER TABLE Customer ADD name LONGTEXT NOT NULL');
        $this->addSql('ALTER TABLE Users ADD address_id INT DEFAULT NULL, ADD phone VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE Users ADD CONSTRAINT FK_D5428AEDF5B7AF75 FOREIGN KEY (address_id) REFERENCES Address (id)');
        $this->addSql('CREATE INDEX IDX_D5428AEDF5B7AF75 ON Users (address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Users DROP FOREIGN KEY FK_D5428AEDF5B7AF75');
        $this->addSql('ALTER TABLE Student DROP FOREIGN KEY FK_789E96AFDB403044');
        $this->addSql('DROP TABLE Address');
        $this->addSql('DROP TABLE Cart');
        $this->addSql('DROP TABLE Student');
        $this->addSql('ALTER TABLE Article DROP FOREIGN KEY FK_CD8737FAF675F31B');
        $this->addSql('DROP INDEX IDX_CD8737FAF675F31B ON Article');
        $this->addSql('ALTER TABLE Article DROP author_id, DROP headline');
        $this->addSql('ALTER TABLE Customer DROP name');
        $this->addSql('DROP INDEX IDX_D5428AEDF5B7AF75 ON Users');
        $this->addSql('ALTER TABLE Users DROP address_id, DROP phone');
    }
}
