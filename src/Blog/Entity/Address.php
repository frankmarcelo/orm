<?php

namespace Blog\Entity;

/**
 * @Entity
 * @Table(name="Address")
 */
class Address
{
    /**
     * @var integer
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
