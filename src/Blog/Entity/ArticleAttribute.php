<?php

namespace Blog\Entity;

/**
 * @Entity
 * @Table(name="ArticleAttribute")
 */
class ArticleAttribute
{
    /** 
     * @var Article[]
     * 
     * @Id 
     * @ManyToOne(targetEntity="Article", inversedBy="attributes") 
     */
    private $article;

    /**
     * var string
     * 
     * @Id 
     * @Column(type="string") 
     */
    private $attribute;

    /**
     * var string
     * 
     * @Column(type="string") 
     */
    private $value;

    public function __construct($name, $value, $article)
    {
        $this->attribute = $name;
        $this->value = $value;
        $this->article = $article;
    }

    /**
     * Set attribute
     *
     * @param string $attribute
     * @return ArticleAttribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute
     *
     * @return string 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ArticleAttribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set article
     *
     * @param \Blog\Entity\Article $article
     * @return ArticleAttribute
     */
    public function setArticle(\Blog\Entity\Article $article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Blog\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
