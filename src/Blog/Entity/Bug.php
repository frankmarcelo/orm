<?php
namespace Blog\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity(repositoryClass="BugRepository")
 * @Table(name="Bugs")
 */
class Bug
{
    /**
     * @var int
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;
    /**
     * @var string
     *
     * @Column(type="string")
     */
    protected $description;
    /**
     * @var \DateTime
     *
     * @Column(type="datetime")
     */
    protected $created;
    /**
     * @Column(type="string")
     * @var string
     */
    protected $status;
    /**
     * @var User
     *
     * @ManyToOne(targetEntity="User", inversedBy="assignedBugs")
     */
    protected $engineer;
    /**
     * @var User
     *
     * @ManyToOne(targetEntity="User", inversedBy="reportedBugs")
     */
    protected $reporter;
    /**
     * @var Product
     *
     * @ManyToMany(targetEntity="Product")
     * @JoinTable(name ="BugProduct")
     */
    protected $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Bug
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Bug
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Bug
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set engineer
     *
     * @param \Blog\Entity\User $engineer
     * @return Bug
     */
    public function setEngineer(\Blog\Entity\User $engineer = null)
    {
        $engineer->assignedToBug($this);
        $this->engineer = $engineer;
    }

    /**
     * Get engineer
     *
     * @return \Blog\Entity\User 
     */
    public function getEngineer()
    {
        return $this->engineer;
    }

    /**
     * Set reporter
     *
     * @param \Blog\Entity\User $reporter
     * @return Bug
     */
    public function setReporter(\Blog\Entity\User $reporter = null)
    {
        $reporter->addReportedBug($this);
        $this->reporter = $reporter;
    }

    /**
     * Get reporter
     *
     * @return \Blog\Entity\User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Add products
     *
     * @param \Blog\Entity\Product $products
     * @return Bug
     */
    public function addProduct(\Blog\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    public function assignToProduct($product)
    {
        $this->products[] = $product;
    }

    /**
     * Remove products
     *
     * @param \Blog\Entity\Product $products
     */
    public function removeProduct(\Blog\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }

    public function close()
    {
        $this->status = "CLOSE";
    }
}
