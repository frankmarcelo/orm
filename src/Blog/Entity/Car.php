<?php

namespace Blog\Entity;

/**
 * @Entity
 * @Table(name="Car")
 */
class Car
{
    /**
     * var string
     *
     * @Id
     * @Column(type="string")
     */
    private $name;

    /**
     * var int
     *
     * @Id
     * @Column(type="integer")
     */
    private $year;

    public function __construct($name, $year)
    {
        $this->name = $name;
        $this->year = $year;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Car
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Car
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }
}
