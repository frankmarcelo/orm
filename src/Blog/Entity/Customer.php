<?php

namespace Blog\Entity;

/**
 * @Entity
 * @Table(name="Customer")
 */
class Customer
{
    /**
     * @var integer
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="text")
     */
    protected $name;

    /**
     * @OneToOne(targetEntity="Cart", mappedBy="customer")
     */
    private $cart;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cart
     *
     * @param \Blog\Entity\Cart $cart
     * @return Customer
     */
    public function setCart(\Blog\Entity\Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return \Blog\Entity\Cart 
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
