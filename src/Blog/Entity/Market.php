<?php

namespace Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Util\Debug;

/**
 * @Entity
 * @Table(name="ExchangeMarkets")
 */
class Market
{
    /**
     * @var int
     *
     * @Id @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="string")
     *
     */
    private $name;

    /**
     * @var Stock[]
     *
     * @OneToMany(targetEntity="Stock", mappedBy="market", indexBy="symbol")
     *
     */
    private $stocks;

    public function __construct($name)
    {
        $this->name = $name;
        $this->stocks = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function addStock(Stock $stock)
    {
        $this->stocks[$stock->getSymbol()] = $stock;
    }

    public function getStock($symbol)
    {
        Debug::dump($symbol);
        if (!isset($this->stocks[$symbol])) {
            throw new \InvalidArgumentException("Symbol is not traded on this market.");
        }

        return $this->stocks[$symbol];
    }

    public function getStocks()
    {
        return $this->stocks->toArray();
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Market
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Remove stocks
     *
     * @param \Blog\Entity\Stock $stocks
     */
    public function removeStock(\Blog\Entity\Stock $stocks)
    {
        $this->stocks->removeElement($stocks);
    }
}
