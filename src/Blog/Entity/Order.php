<?php

namespace Blog\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Blog\Entity\Customer;

/**
 *
 * @Entity
 * @Table(name="Order")
 */
class Order
{
    /**
     * @var integer
     *
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     *
     * @ManyToOne(targetEntity="Customer")
     */
    private $customer;

    /**
     *
     * @OneToMany(targetEntity="OrderItem", mappedBy="order")
     */
    private $items;

    /**
     * @var bool
     *
     * @Column(type="boolean")
     */
    private $payed = false;

    /**
     * @var bool
     *
     * @Column(type="boolean")
     */
    private $shipped = false;

    /**
     * @var DateTime
     *
     * @Column(type="datetime")
     */
    private $created;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
        $this->items = new ArrayCollection();
        $this->created = new \DateTime("now");
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     * @return Order
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return boolean 
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set shipped
     *
     * @param boolean $shipped
     * @return Order
     */
    public function setShipped($shipped)
    {
        $this->shipped = $shipped;

        return $this;
    }

    /**
     * Get shipped
     *
     * @return boolean 
     */
    public function getShipped()
    {
        return $this->shipped;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Order
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    } 

    /**
     * Set customer
     *
     * @param \Blog\Entity\Customer $customer
     * @return Order
     */
    public function setCustomer(\Blog\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \Blog\Entity\Customer 
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Add items
     *
     * @param \Blog\Entity\OrderItem $items
     * @return Order
     */
    public function addItem(\Blog\Entity\OrderItem $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \Blog\Entity\OrderItem $items
     */
    public function removeItem(\Blog\Entity\OrderItem $items)
    {
        $this->items->removeElement($items);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }
}
