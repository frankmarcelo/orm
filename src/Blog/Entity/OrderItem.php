<?php

namespace Blog\Entity;
/**
 *
 * @Entity
 * @Table(name="OrderItem")
 */
class OrderItem
{
    /**
     *
     * @Id
     * @ManyToOne(targetEntity="Order")
     */
    private $order;

    /**
     *
     * @Id
     * @ManyToOne(targetEntity="Product")
     */
    private $product;

    /**
     * @var integer
     *
     * @Column(type="integer")
     */
    private $amount = 1;

    /**
     * @var decimal
     *
     * @Column(type="decimal")
     */
    private $offeredPrice;

    public function __construct(Order $order, Product $product, $amount = 1)
    {
        $this->order = $order;
        $this->product = $product;
        $this->offeredPrice = $product->getCurrentPrice();
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return OrderItem
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set offeredPrice
     *
     * @param string $offeredPrice
     * @return OrderItem
     */
    public function setOfferedPrice($offeredPrice)
    {
        $this->offeredPrice = $offeredPrice;

        return $this;
    }

    /**
     * Get offeredPrice
     *
     * @return string 
     */
    public function getOfferedPrice()
    {
        return $this->offeredPrice;
    }

    /**
     * Set order
     *
     * @param \Blog\Entity\Order $order
     * @return OrderItem
     */
    public function setOrder(\Blog\Entity\Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Blog\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set product
     *
     * @param \Blog\Entity\Product $product
     * @return OrderItem
     */
    public function setProduct(\Blog\Entity\Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Blog\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
