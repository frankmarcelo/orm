<?php

namespace Blog\Entity;

/**
 * @Entity
 * @Table(name="ExchangeStocks")
 */
class Stock
{
    /**
     * @var int
     *
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * For real this column would have to be unique=true. But I want to test behavior of non-unique overrides.
     *
     * @Column(type="string", unique=true)
     */
    private $symbol;

    /**
     * @var Market
     *
     * @ManyToOne(targetEntity="Market", inversedBy="stocks")
     */
    private $market;

    public function __construct($symbol, Market $market)
    {
        $this->symbol = $symbol;
        $this->market = $market;
        $market->addStock($this);
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return Stock
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Set market
     *
     * @param \Blog\Entity\Market $market
     * @return Stock
     */
    public function setMarket(\Blog\Entity\Market $market = null)
    {
        $this->market = $market;

        return $this;
    }

    /**
     * Get market
     *
     * @return \Blog\Entity\Market 
     */
    public function getMarket()
    {
        return $this->market;
    }
}
