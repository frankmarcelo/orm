<?php

namespace Blog\Entity;

/**
 * @Entity
 * @Table(name="Student")
 */
class Student
{
    /**
     * @var int
     *
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Column(type="text")
     */
    protected $name;

    /**
     * @OneToOne(targetEntity="Student")
     * @JoinColumn(name="mentor_id", referencedColumnName="id")
     */
    private $mentor;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mentor
     *
     * @param \Blog\Entity\Student $mentor
     * @return Student
     */
    public function setMentor(\Blog\Entity\Student $mentor = null)
    {
        $this->mentor = $mentor;

        return $this;
    }

    /**
     * Get mentor
     *
     * @return \Blog\Entity\Student 
     */
    public function getMentor()
    {
        return $this->mentor;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Student
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Student
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
