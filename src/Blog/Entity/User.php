<?php
namespace Blog\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity
 * @Table(name="Users")
 */
class User
{
    /**
     * @var integer
     *
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     */
    protected $id;
    /**
     * @var string
     *
     * @Column(type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @Column(type="string", nullable=true)
     */
    protected $phone;
    /**
     * @var Bug[]
     *
     * @OneToMany(targetEntity="Bug", mappedBy="reporter")
     **/
    protected $reportedBugs;
    /**
     * @var Bug[]
     *
     * @OneToMany(targetEntity="Bug", mappedBy="engineer")
     **/
    protected $assignedBugs;

    /**
     * @var 
     *
     * @ManyToOne(targetEntity="Address")
     */
    private $address;

    public function __construct()
    {
        $this->reportedBugs = new ArrayCollection();
        $this->assignedBugs = new ArrayCollection();
    }

    public function addReportedBug($bug)
    {
        $this->reportedBugs[] = $bug;
    }

    public function assignedToBug($bug)
    {
        $this->assignedBugs[] = $bug;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Remove reportedBugs
     *
     * @param \Blog\Entity\Bug $reportedBugs
     */
    public function removeReportedBug(\Blog\Entity\Bug $reportedBugs)
    {
        $this->reportedBugs->removeElement($reportedBugs);
    }

    /**
     * Get reportedBugs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportedBugs()
    {
        return $this->reportedBugs;
    }

    /**
     * Add assignedBugs
     *
     * @param \Blog\Entity\Bug $assignedBugs
     * @return User
     */
    public function addAssignedBug(\Blog\Entity\Bug $assignedBugs)
    {
        $this->assignedBugs[] = $assignedBugs;

        return $this;
    }

    /**
     * Remove assignedBugs
     *
     * @param \Blog\Entity\Bug $assignedBugs
     */
    public function removeAssignedBug(\Blog\Entity\Bug $assignedBugs)
    {
        $this->assignedBugs->removeElement($assignedBugs);
    }

    /**
     * Get assignedBugs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssignedBugs()
    {
        return $this->assignedBugs;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set address
     *
     * @param \Blog\Entity\Address $address
     * @return User
     */
    public function setAddress(\Blog\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \Blog\Entity\Address 
     */
    public function getAddress()
    {
        return $this->address;
    }
}
