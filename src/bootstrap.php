<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\AnsiQuoteStrategy;
use Doctrine\Common\EventManager;
use Blog\Event\InsultEventListener;
use Blog\Event\MailAuthorOnCommentEventSubscriber;

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../config/config.php';

$entitiesPath = array(__DIR__.'/Blog/Entity', __DIR__ . '/Weather');

$eventManager = new EventManager();
$eventManager->addEventListener([Events::prePersist], new InsultEventListener());
$eventManager->addEventSubscriber(new MailAuthorOnCommentEventSubscriber());

$db = \Doctrine\DBAL\DriverManager::getConnection($dbParams);
$config = Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration($entitiesPath, $dev);
$config->setQuoteStrategy(new AnsiQuoteStrategy());
$config->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());
$entityManager = Doctrine\ORM\EntityManager::create($dbParams, $config, $eventManager);

