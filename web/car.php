<?php

require_once __DIR__ . '/../src/bootstrap.php';

use Blog\Entity\Car;

$car = new Car("Audi A8", 2010);
$entityManager->persist($car);
$entityManager->flush();