<?php

require_once __DIR__ . '/../src/bootstrap.php';

$theBugId = $argv[1];

$bug = $entityManager->find("Blog\Entity\Bug", (int)$theBugId);
$bug->close();

$entityManager->flush();