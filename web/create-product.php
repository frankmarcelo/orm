<?php
// create_product.php
require_once __DIR__ . '/../src/bootstrap.php';

use Blog\Entity\Product;

$newProductName = $argv[1];

$product = new Product();
$product->setName($newProductName);

$entityManager->persist($product);
$entityManager->flush();

echo "Created Product with ID " . $product->getId() . "\n";