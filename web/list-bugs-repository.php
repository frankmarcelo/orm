<?php

require_once __DIR__ . '/../src/bootstrap.php';

$bugs = $entityManager->getRepository('Blog\Entity\Bug')->getRecentBugs();

foreach ($bugs as $bug) {
    echo $bug->getDescription()." - ".$bug->getCreated()->format('d.m.Y')."<br>";
    echo "    Reported by: ".$bug->getReporter()->getName()."<br>";
    echo "    Assigned to: ".$bug->getEngineer()->getName()."<br>";
    foreach ($bug->getProducts() as $product)
    {
        //$productCount = $entityManager->getRepository('Blog\Entity\Product')
        //    ->count(['name' => $product->getName()]);

        $productCount = 1;
        echo "    Platform: ".$product->getName()."=========".$productCount."<br>";
    }
    echo "<br>";
}


