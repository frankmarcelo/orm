<?php

require_once __DIR__ . '/../src/bootstrap.php';

$productRepository = $entityManager->getRepository('Blog\Entity\Product');
$products = $productRepository->findAll();

foreach ($products as $product) {
    echo sprintf("-%s\n", $product->getName());
}