<?php

require_once __DIR__ . '/../src/bootstrap.php';
// $em is the EntityManager

$market = new Blog\Entity\Market("Some Exchange");
$stock1 = new Blog\Entity\Stock("AAPL", $market);
$stock2 = new Blog\Entity\Stock("GOOG", $market);

$entityManager->persist($market);
$entityManager->persist($stock1);
$entityManager->persist($stock2);
$entityManager->flush();