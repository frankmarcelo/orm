<?php
/**
 * Created by IntelliJ IDEA.
 * User: frankitoy
 * Date: 4/11/16
 * Time: 3:46 AM
 */
require_once __DIR__ . '/../src/bootstrap.php';

$dql = "SELECT p.id, p.name, count(b.id) AS openBugs FROM Blog\Entity\Bug b ".
    "JOIN b.products p WHERE b.status = 'OPEN' GROUP BY p.id";
$productBugs = $entityManager->createQuery($dql)->getScalarResult();

foreach ($productBugs as $productBug) {
    echo $productBug['name']." has " . $productBug['openBugs'] . " open bugs!<br><br>";
}