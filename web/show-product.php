<?php

require_once __DIR__ . '/../src/bootstrap.php';

$id = $argv[1];
$product = $entityManager->find('Blog\Entity\Product', $id);

// Doctrine\Common\Util\Debug::dump($product);
if ($product === null) {
    echo "No product found.\n";
    exit(1);
}

echo sprintf("-%s\n", $product->getName());