<?php
// $em is the EntityManager
require_once __DIR__ . '/../src/bootstrap.php';

$marketId = 3;
$symbol = "AAPL";

$dql = "SELECT m, s FROM Blog\Entity\Market m JOIN m.stocks s WHERE m.id = ?1";
$market = $entityManager->createQuery($dql)
    ->setParameter(1, $marketId)
    ->getSingleResult();

// Access the stocks by symbol now:
$stock = $market->getStock($symbol);

echo $stock->getSymbol(); // will print "AAPL"