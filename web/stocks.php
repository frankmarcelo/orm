<?php

require_once __DIR__ . '/../src/bootstrap.php';

// $em is the EntityManager
$marketId = 3;
$symbol = "AAPL";

$market = $entityManager->find("Blog\Entity\Market", $marketId);
//Doctrine\Common\Util\Debug::dump($market);


// Access the stocks by symbol now:
$stock = $market->getStock($symbol);

echo $stock->getSymbol(); // will print "AAPL"