<?php

require_once __DIR__ . '/../src/bootstrap.php';

$service = new \Weather\Service();
$weather = new \Weather\Weather();
try {
    echo $weather->fetchData($service)
        ->getSummary() . PHP_EOL;
} catch (\Exception $e) {
    echo 'ERROR: ' . $e->getMessage() . PHP_EOL;
}